if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this == null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}
if (!Array.prototype.findIndex) {
  Array.prototype.findIndex = function(predicate) {
    if (this == null) {
      throw new TypeError('Array.prototype.findIndex called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return i;
      }
    }
    return -1;
  };
}

var studentsAndPoints = [
		'Алексей Петров', 0,
		'Ирина Овчинникова', 60,
		'Глеб Стукалов', 30,
		'Антон Павлович', 30,
		'Виктория Заровская', 30,
		'Алексей Левенец', 70,
		'Тимур Вамуш', 30,
		'Евгений Прочан', 60,
		'Александр Малов', 0
	],
	students = [],
	showStudent = function() {
		console.log('Студент %s набрал %d баллов', this.name, this.point);
	};

for (i = 0, imax = studentsAndPoints.length; i < imax; i += 2) {
	students.push({
		name  : studentsAndPoints[i],
		point : studentsAndPoints[i + 1],
		show  : showStudent
	});
}

students.push({
	name  : 'Николай Фролов',
	point : 0,
	show  : showStudent
}, {
	name  : 'Олег Боровой',
	point : 0,
	show  : showStudent
});

students.addPoints = function(studentName, points) {
	var objIndex = this.findIndex(function (obj) {
		return obj.name == studentName;
	});
	students[objIndex].point += points;
};
students.addPoints('Ирина Овчинникова', 30);
students.addPoints('Александр Малов', 30);
students.addPoints('Николай Фролов', 10);

var topStudents = students.filter(function (obj, i) {
		return obj.point >= 30;
	});
console.log('Список студентов:');
topStudents.forEach(function (obj, i) {
	obj.show();
});

students.forEach(function (obj, i) {
	obj.worksAmount = obj.point / 10;
});

students.findByName = function(studentName) {
	return this.find(function (obj) {
		return obj.name == studentName;
	});
};
console.log(students.findByName('Николай Фролов'));